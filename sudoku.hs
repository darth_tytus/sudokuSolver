module Main where

import Data.List
import Data.Ix(range)
import Data.Maybe(fromJust)

type Grid   = [[Char]]
type Square = (Int, Int)
type Digit  = Char


empty :: Digit -> Bool
empty = (== '0')

digits :: String
digits = ['1'.. '9']

number_of_tests :: Int
number_of_tests = 50

main :: IO ()
main = readFile "50sudoku.txt" >>= putStrLn . show . solveEuler


solveEuler :: String -> Int
solveEuler = sum . extract . solve . parse
  where
    parse   = map tail . chunksOf 10 . lines
    solve   = map (fromJust . singleSolution)
    extract = map (read . take 3 . head)
    

verifySolution :: Grid -> Grid -> Grid
verifySolution rPuzzle rSolution =
  let
    solution = concat rSolution
    puzzle   = concat rPuzzle
    allGood  = all id (zipWith (\s p -> if (not . empty) p then s == p else s `elem` digits) solution puzzle)
  in
    case allGood  of
      True  -> rSolution
      False -> error "Invalid solution"

side_index :: [Int]
side_index = [0.. 8]

-- all possible coordinates
coords :: [(Int, Int)]
coords = [(x, y) | x <- side_index, y <- side_index]


-- pairs : (coord, his friend)
peers :: [(Square, [Square])] 
peers =
  let
    all_rows  = chunksOf 9 coords
    all_cols  = transpose all_rows
    all_boxes = [range ((x, y), (x + 2, y + 2)) | x <- [0, 3, 6], y <- [0, 3, 6]]
    all_units = all_rows ++ all_cols ++ all_boxes :: [[Square]]
  in
    do
      coord <- coords
      pure (coord, nub $ concat [unit | unit <- all_units, coord `elem` unit])
    

-- This is function is not inverse of the next one.
rawStringToGrid :: String -> Grid
rawStringToGrid = chunksOf 9

-- break string into chunks of size n.
chunksOf :: Int -> [a] -> [[a]]
chunksOf _ [] = []
chunksOf n xs = take n xs : chunksOf n (drop n xs)

  
gridToString :: Grid -> String
gridToString = (++"\n") . concatMap ((++ "\n") . intersperse ' ')

singleSolution :: Grid -> Maybe Grid
singleSolution grid = case allSolutions grid of
                        (x:_) -> Just x
                        _     -> Nothing


allSolutions :: Grid -> [Grid]
allSolutions = search

-- Oh my! list monad is awesome. 
search :: Grid -> [Grid]
search grid =
    case nextEmpty grid of
      Nothing
        ->
        if isFull grid
        then
          [grid]
        else
          []

      Just sq
        ->
        branches >>= search 
        where
          branches = map (makeMove grid sq) (possibleDigits grid sq)


-- Returns next empty square to be filled.
nextEmpty :: Grid -> Maybe Square
nextEmpty board =
  case [i | (i, v) <- zip coords (concat board), empty v] of
    (x:_) -> Just x
    _     -> Nothing

isFull :: Grid -> Bool
isFull = all (not . empty) . concat

-- Makes move and return new (modified) grid
makeMove :: Grid -> Square -> Digit -> Grid
makeMove grid (row, col) digit =
  let
    modRow  = grid !! row
    modd    = take col modRow ++ [digit] ++ drop (col + 1) modRow
  in
    take row grid ++ [modd] ++ drop (row + 1) grid
  
-- all digits that can fill given square
possibleDigits :: Grid -> Square -> [Digit]
possibleDigits grid square = [x | x <- digits,
                              x `notElem` unitVals grid square]
  where
    unitVals :: Grid -> Square -> [Digit]
    unitVals g sq =
      let
        sqPeers = fromJust $ lookup sq peers
      in
        nub [d | (coord, d) <- zip coords (concat g), coord `elem` sqPeers]
